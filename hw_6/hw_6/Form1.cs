﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace hw_6
{
    public partial class login_form : Form
    {
        public login_form()
        {
            InitializeComponent();
        }

        private void username_lbl_Click(object sender, EventArgs e)
        {

        }

        private void login_btn_Click(object sender, EventArgs e)
        {
            var users_map = new Dictionary<string, string>();
            using (StreamReader sr = new StreamReader("Users.txt")) 
            {
                while (sr.Peek() >= 0)
                {
                    string user = sr.ReadLine();
                    int index = user.IndexOf(',');
                    string username = user.Substring(0, index);
                    string password = user.Substring(index + 1, user.Length - index - 1);
                    users_map.Add(username, password);
                }
            }
            if(users_map.ContainsKey(username_txtbx.Text) && users_map[username_txtbx.Text] == password_txtbx.Text)
            {
                bdays f = new bdays();
                f.Username = username_txtbx.Text;
                f.createBDmap();
                this.Hide();
                f.ShowDialog();
                this.Show();
            }
            else
            {
                MessageBox.Show("Username or Password wrong");
            }
        }

        private void exit_btn_Click(object sender, EventArgs e)
        {
            MessageBox.Show("BYE BYE");
            this.Close();
        }

        private void password_txtbx_TextChanged(object sender, EventArgs e)
        {
            password_txtbx.PasswordChar = '*';
            password_txtbx.TextAlign = HorizontalAlignment.Center;
        }

        private void username_txtbx_TextChanged(object sender, EventArgs e)
        {
            username_txtbx.TextAlign = HorizontalAlignment.Center;
        }
    }
}
