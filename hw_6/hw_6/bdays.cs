﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace hw_6
{
    public partial class bdays : Form
    {
        private string _userName;
        Dictionary<string, string> bd_map = new Dictionary<string, string>() {};

        public bdays()
        {
            InitializeComponent();
        }

        public string Username
        {
            get { return _userName; }
            set { _userName = value; }
        }

        private void bdays_Load(object sender, EventArgs e)
        {
              
        }

        public void createBDmap()
        {
            string path = this._userName + "BD.txt";
            using (StreamWriter w = File.AppendText(path)) //if file doesn't exists it create new one
            {
                w.Close();
            }

            using (StreamReader sr = new StreamReader(path))
            {
                while (sr.Peek() >= 0)
                {
                    string friend = sr.ReadLine();
                    int index = friend.IndexOf(',');
                    string name = friend.Substring(0, index);
                    string bday = friend.Substring(index + 1, friend.Length - index - 6);
                    bd_map.Add(name, bday);
                }
                sr.Close();
            }
        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            string dateChosen = monthCalendar1.SelectionRange.Start.Date.ToString("M/d");
            DateTime localDate = DateTime.Now;
            string currDate = localDate.ToString("M/d");
            if (this.bd_map.ContainsValue(dateChosen))
            {
                msg_lbl.Text = "The chosen date is " + bd_map.FirstOrDefault(x => x.Value == dateChosen).Key +"'s birthday";
            }
            else if(dateChosen == currDate)
            {
                msg_lbl.Text = "That's today's date!";
            }
            else
            {
                msg_lbl.Text = "Nothing special....";
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void bdays_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }
    }
}
